var sql = require("../config/mysql_db.js");

exports.saveSession=function(session){
    return new Promise((resolve, reject) => {
        sql.query("INSERT INTO sessions SET ?", session, (err, rows) => {
            if (err) reject(new Error("Error SQL:" + err))
            else { resolve(rows.changedRows); }
        })
    })
}

exports.save=function(transaction){
    return new Promise((resolve, reject) => {
        sql.query("INSERT INTO transactions SET ?", transaction, (err, rows) => {
            if (err) reject(new Error("Error SQL:" + err))
            else { resolve(rows.changedRows); }
        })
    })
}

exports.countBy=function(conditions){
    return new Promise((resolve, reject) => {
        sql.query(`SELECT count(*) as count FROM transactions t inner join users u on u.id=t.user_id WHERE ${conditions}`, (err, rows) => {
            if (err) reject(new Error("Error SQL:" + err))
            else { resolve(rows[0].count); }
        })
    })
}

exports.getBy=function(conditions){
    return new Promise((resolve, reject) => {
        sql.query(`SELECT t.* FROM transactions t inner join users u on u.id=t.user_id WHERE ${conditions}`, (err, rows) => {
            if (err) reject(new Error("Error SQL:" + err))
            else { resolve(rows); }
        })
    })
}

exports.updateUser=function(user){
    return new Promise((resolve, reject) => {
        const query=`UPDATE users SET name=?, balance=?, serial=?  WHERE id=?`;
        console.log("SQL",query,user);
        sql.query(query,[user.name, user.balance, user.serial,user.id], (err, rows) => {
            if (err) reject(new Error("Error SQL:" + err))
            else {console.log(`Changed ${rows.changedRows} row(s)`); resolve(rows); }
        })
    })
}

exports.getUser=function(user_id){
    return new Promise((resolve, reject) => {
        sql.query(`select * from users where id = ${user_id} `, (err, rows) => {
            if (err) reject(new Error("Error SQL:" + err))
            else { if( rows.length ) resolve(rows[0]); else resolve(null); }
        })
    })
}

exports.getSessionBy=function(conditions){
    return new Promise((resolve, reject) => {
        sql.query(`select s.* from sessions s where ${conditions} `, (err, rows) => {
            if (err) reject(new Error("Error SQL:" + err))
            else { resolve(rows);  }
        })
    })
}

exports.updateTransaction=function(old_trx,new_trx){
    return new Promise((resolve, reject) => {
        const query=`UPDATE transactions SET trxid='${new_trx}'  WHERE trxid='${old_trx}'`;
        console.log("SQL",query);
        sql.query(query, (err, rows) => {
            if (err) reject(new Error("Error SQL:" + err))
            else {console.log(`Changed ${rows.changedRows} row(s)`); resolve(rows); }
        })
    })
}
exports.getUserBy=function(conditions){
    return new Promise((resolve, reject) => {
        sql.query(`select u.* from users u where ${conditions} `, (err, rows) => {
            if (err) reject(new Error("Error SQL:" + err))
            else { resolve(rows);  }
        })
    })
}
