var sql = require("../config/mysql_db.js");
const axios = require('axios');
const UNIVERSAL = 'https://test.apiuniversalsoft.com/api';
// const UNIVERSAL = 'http://localhost:8042/api';

exports.reporter = async function (params) {
    let data = await axios.post(`${UNIVERSAL}/api/reporter`, params);
    return data;
}
exports.reporterbyuser = async function (params) {
    let data = await axios.post(`${UNIVERSAL}/api/reporterbyuser`, params);
    return data;
}
exports.getProviders = async function (universalapi) {
    let {data} = await axios.post(`${universalapi}/api/providers`,{ok:'ok'});
    return data;
}
exports.reporterProvider = async function (params,universalapi) {
    let {data} = await axios.post(`${universalapi}/api/reportProv`,params) ;
    return data ;
}