require("dotenv").config();
var path = require('path');
var express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cors = require('cors');

var compression = require('compression')
const transactionRepository = require("./repository/transaction");
const reporterRepository = require("./repository/reporter");
const { response } = require('express');
//const TOKEN_CLIENT_REQUEST = "05978C65BF2328FA53493A1BFTESTAPI";
const client_phrase_demo = process.env.CLIENT_PHRASE_DEMO;
const LOBBY_AUTH_HEADERS = {auth: client_phrase_demo };

var userdemo = "demo";

var app = express();
app.use(cors());
const DEFAULT_USER_ID = 1;
let date_init;
let date_end;
let provider;

const DEV_ENV_API_1 = "https://test.apiuniversalsoft.com";
const PROD_ENV_API_1 = "https://apiuniversalsoft.com";

const DEV_ENV_API="https://test-2.apiusoft.com";
const PROD_ENV_API="https://prd.apiusoft.com";

const LOBBY_TEST_URL = "https://lobby-test.apiusoft.com";
// const LOBBY_TEST_URL = "https://test-2.apiusoft.com";
const LOBBY_PROD_URL = "https://lobby.apiusoft.com";

const DEV_JAVA_URL = "https://test-2.apiusoft.com";
const PROD_JAVA_URL = "https://test-2.apiusoft.com";

let universalapi = '' ;
app.use(compression());
app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded({ extended: true }));
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get("/",async function(req, res){
  res.redirect('https://demo.apiuniversalsoft.com/lobby');
});

app.get('/lobby', async function (req, res) {
  try {
    const env = req.query.env || 'dev';
    const universalapi = env == 'prod' ? PROD_JAVA_URL : DEV_JAVA_URL  ;
    console.log("API ENV:", universalapi);
    let user_id = req.query.user_id || DEFAULT_USER_ID;
    let user = await transactionRepository.getUser(user_id);
    if(!user.serial){
      let user_params = {
        actionAPI: "YSetUser",
        type:'WEB',
        login:user.username,
        pass:"free",
        names:user.name,
        surnames:user.lastname,
        sex:'M',
        bdate:'20010101',
        doctype:'DNI',
        docnum:'123123',
        address:'Jr. Demo',
        zip:'',
        city:'TM',
        country:'PE',
        phone:'',
        email:'',
        web:'',
        image:'',
        currency:'',
        balance:user.balance
      };

      let data = null;
      try {

        data = await axios.post(universalapi + "/UniversalAPI", user_params,{ headers: LOBBY_AUTH_HEADERS });
        console.log("data",data);
        user.serial = data.uSerial;
        await transactionRepository.updateUser(user);
      } catch (error) {
        console.log("Error crear usuario ",error);
        data = error;
      }
    }

    let session_params={actionAPI:'YStartSession',userial:user.serial,ubalance:user.balance};
    console.log("session_params",session_params);

    let { data } = await axios.post(universalapi + "/UniversalAPI", session_params,{ headers: LOBBY_AUTH_HEADERS });
    console.log("data",data);//return a uSerial
    let user_token =data.utoken;
    let session = { user_id: user.id, sessionid: user_token };
    await transactionRepository.saveSession(session);

    let lobby_url = (env=='prod'?LOBBY_PROD_URL:LOBBY_TEST_URL)+'?t='+user_token;
    console.log("initgame URL lobby:", lobby_url);
    res.render('lobby', { env, user, lobby_url });
  } catch (error) {
    let response ={};
    if(error.response) response= error.response.data;
    else response= error;
    console.log("error", response);
    res.status(500).json({error:response});
  }
});

app.get('/v2', async function (req, res) {
  try {
    const user_id = req.query.user_id || DEFAULT_USER_ID;
    const env = req.query.env || 'dev';
    const universalapi = env == 'prod' ? PROD_ENV_API_1 : DEV_ENV_API_1 ;
    console.log("API env:", universalapi);
    const { data } = await axios.get(universalapi + "/api/gamefilter", { headers: { phrase: client_phrase_demo } });
    res.render('listgames2', { env: env, user_id: user_id, endpoint: universalapi, brands: data.brands, categories: data.categories, l: [] })
  } catch (error) {
    console.log("Error:", JSON.stringify(error));
    res.end(error);
  }
});
app.get('/v2/m', async function (req, res) {
  try {
    const user_id = req.query.user_id || DEFAULT_USER_ID;
    const env = req.query.env || 'dev';
    const universalapi = env == 'prod' ? PROD_ENV_API_1 : DEV_ENV_API_1 ;
    const { data } = await axios.get(universalapi + "/api/gamefilter", { headers: { phrase: client_phrase_demo } })

    res.render('listgames_mobile', { env: env, user_id: user_id, endpoint: universalapi, brands: data.brands, categories: data.categories, l: [] })
  } catch (error) {
    res.end(error);
  }
});

app.get('/v_test', async function (req, res) {
  try {
    const user_id = req.query.user_id || DEFAULT_USER_ID;
    const env = req.query.env || 'dev';
    const universalapi = env == 'prod' ? PROD_ENV_API_1 : DEV_ENV_API_1 ;
    const { data } = await axios.get(universalapi + "/api/gamefilter", { headers: { phrase: client_phrase_demo } });
    res.render('listgames_test', { env: env, user_id: user_id, endpoint: universalapi, brands: data.brands, categories: data.categories, l: [] })
  } catch (error) {
    res.end(error);
  }
});

app.get("/gamelist", async function (req, res) {
  try {
    const env = req.query.env || 'dev';
    const universalapi = env == 'prod' ? PROD_ENV_API_1 : DEV_ENV_API_1 ;
    console.log("listando juegos desde:", universalapi);
    let params = req.query;
    delete params.env;
    var query_params = '';
    for (const prop in params) { query_params += `&${prop}=${params[prop]}`; }
    query_params = query_params.replace("&", "?");

    const { data } = await axios.get(universalapi + "/api/gamelist" + query_params, "", { headers: { 'Content-Type': 'application/json' } }, { maxContentLength: 52428890 });
    console.log("Juegos en Demo: ", JSON.stringify(data));
    // wss://prelive-dga.pragmaticplaylive.net/ws
    //if()
    res.json(data);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
})

app.get('/initgame', async function (req, res) {
  const user_id = req.query.user_id || DEFAULT_USER_ID;
  const user = await transactionRepository.getUser(user_id);
  let params = { username: user.username, balance: user.balance };
  try {
    const env = req.query.env || 'dev';
    const universalapi = env == 'prod' ? PROD_ENV_API_1 : DEV_ENV_API_1 ;
    console.log("abriendo juego desde:", universalapi);
    const { data } = await axios.post(universalapi + "/api/auth", params);
    let tokenData = data;
    const session = { user_id: user.id, sessionid: tokenData.val.token };
    await transactionRepository.saveSession(session);
    let p = req.query.p;
    let b = req.query.b;
    let g = req.query.g;
    let u = params.username;
    let m = req.query.m;
    const url = universalapi + "/api/launch?gameid=" + g + "&p=" + p + "&b=" + b + "&m=" + m + "&sessionid=" + tokenData.val.token;
    console.log("initgame URL", url);
    let html = (await axios.get(url)).data;
    res.send(html);
  } catch (e) {
    console.log("ERROR", e);
    res.send({ 'error': e });
  }
});
//update token
app.post('/refreshToken', async function (req, res) {
  const params = req.body;
  console.log("Params Refresh Token: ", params);
  try {
    const session = (await transactionRepository.getSessionBy(`sessionid='${params.oldtoken}'`))[0];
    if (!session) throw "User not found " + params.token;
    const session_save = { user_id: session.user_id, sessionid: params.newtoken };
    let resp = await transactionRepository.saveSession(session_save);
    let resp_json = { ok: 'ok',status:1,newtoken: params.newtoken };
    res.json(resp_json);
  } catch (e) {
    console.log("getUserBalance params", JSON.stringify(params), "ERROR", e);
    res.status(500).send({ status: 0, message: e });
  }
});
app.post('/updateTrxId', async function (req, res) {
  const params = req.body;
  console.log("Params Update Balance: ", params);
  try {
    const trx_update = (await transactionRepository.updateTransaction(params.oldTransaction,params.newTransaction));
    console.log("trx ))>> ",JSON.stringify(trx_update));
    res.json({ status: 1 });
  } catch (e) {
    console.log("update trxid", e);
    res.status(500).send({ status: 0 });
  }
});
app.get('/credit', async function (req, res) {
  try {
    const user_id = req.query.user_id;
    credit = req.query.credit;
    const user = await transactionRepository.getUser(user_id);
    const new_balance = user.balance + credit * 1;
    await transactionRepository.updateUser({ id: user_id, balance: new_balance, sessionid: user.sessionid });
    res.redirect("transactions?user_id=" + user_id)
  } catch (error) {
    res.end(error);
  }
});
app.get('/transactions', async function (req, res) {
  try {
    user_id = req.query.user_id || 1;
    page = req.query.page || 1;
    xpage = 30;
    const offset = xpage * (page - 1);
    const user = await transactionRepository.getUser(user_id);
    const list = await transactionRepository.getBy(`u.id='${user_id}'  order by 1 desc limit ${offset},${xpage}`);
    const total = await transactionRepository.countBy(`u.id='${user_id}'`);
    let pages = [];
    for (var i = 1; i <= Math.ceil(total / xpage); i++) {
      pages.push(i);
    }
    res.render('transactions', { list, pages, page, user })
  } catch (error) {
    console.log(error);
    res.send(error); res.end();
  }
});
//request by Provider
app.post('/insertBet', async function (req, res) {
  console.log("insertBet params", JSON.stringify(req.body));
  const trx = req.body;
  try {
    if (!trx.usertoken || !trx.gameid || !trx.trxid) throw `Some fields undefined in ${JSON.stringify(trx)}`;
    //trx.amount=trx.amount*-1; // un bet siempre viene en negativo
    const session = (await transactionRepository.getSessionBy(`sessionid='${trx.usertoken}'`))[0];
    if (!session) throw `USER NOT FOUND sessionid:${trx.usertoken}`;
    trx.user_id = session.user_id;
    delete trx.usertoken;
    await transactionRepository.save(trx);

    let user = await transactionRepository.getUser(session.user_id);
    user.balance += trx.amount;
    await transactionRepository.updateUser(user);
    const response = { status: 1, balance: user.balance };
    console.log("insertBet response", JSON.stringify(response));
    res.send(response);
  } catch (e) {
    console.log("ERROR", e);
    res.status(500).send({ status: 0, message: e });
  }
});
app.post('/BetInsert', async function (req, res) {//apiusoft
  console.log("apiusoft/BetInsert params", JSON.stringify(req.body));
  const trx = req.body;
  try {
    if( trx.usertoken =='' ||  trx.usertoken == null) throw "USERTOKEN_VOID";
    if( trx.trxid =='' ||  trx.trxid == null) throw "TRXID_VOID";
    if( trx.gameid =='' ||  trx.gameid == null) throw "GAMEID_VOID";
    const session = (await transactionRepository.getSessionBy(`sessionid='${trx.usertoken}'`))[0];
    if (!session) throw `USER_NOT_FOUND`;
    trx.user_id = session.user_id;
    let user = await transactionRepository.getUser(session.user_id);
    if(user.status==0) throw "INACTIVE_USER";
    if(trx.amount >= 0) throw 'BAD_AMOUNT';
    if(trx.amount*-1 > user.balance ) throw 'INSUFFICIENT_FUNDS';
    delete trx.sessionid;
    delete trx.usertoken;
    duplicate_trx = (await transactionRepository.getBy(`trxid='${trx.trxid}'`))[0];
    if(duplicate_trx) throw 'TRX_DUPLICATED';
    console.log("TRX",trx);
    await transactionRepository.save(trx);
    
    if(user.balance < trx.amount) throw "INSUFICIENT_BALANCE";
    user.balance += trx.amount;
    await transactionRepository.updateUser(user);
    const response = { status: 1, balance: user.balance, code:'OK-00' };
    console.log("apiusoft/BetInsert response",  JSON.stringify(response));
    res.send(response);
  } catch (e) {
    console.log(e);
    let response = { status: 0, message: e , code:'ERR-00'};
    if(e == 'USER_NOT_FOUND') response.code='BET-01';
    else if(e == 'INACTIVE_USER') response.code='USER-04';
    else if(e == 'BAD_AMOUNT') response.code='BET-05';
    else if(e == 'USERTOKEN_VOID') response.code='REQ-05';
    else if(e == 'INSUFICIENT_BALANCE') response.code='USER-03';
    else if(e == 'TRX_DUPLICATED') response.code='BET-03';
    else if(e == 'TRXID_VOID') response.code='REQ-05';
    else if(e == 'GAMEID_VOID') response.code='REQ-05';
    else if(e == 'INSUFFICIENT_FUNDS') response.code='USER-03';
    res.status(500).send(response);
  }
});
//request by Provider
app.post('/betResult', async function (req, res) {
  let trx = req.body;
  try {
    if (!trx.usertoken || !trx.gameid || !trx.trxid) throw `Some fields undefined in ${JSON.stringify(trx)}`;
    let db_trx = null;
    if (trx.trxid == trx.referenceBet) {//vino el bet y win en la misma trx.
      db_trx = { movement: 'BET' };
    } else {
      db_trx = (await transactionRepository.getBy(`trxid='${trx.referenceBet}'`))[0];
      // console.log("Transaction ",db_trx);
      if (!db_trx) throw `Ref. ${trx.referenceBet} not found`;
    }
    //if(trx.movement=='BET') trx.amount=trx.amount*-1; //-
    if (trx.movement == 'WIN') trx.amount = trx.amount * 1;//+
    else if (trx.movement == 'REFUND' && db_trx.movement == 'BET') trx.amount = trx.amount * 1;//refund bet +
    else if (trx.movement == 'REFUND' && db_trx.movement == 'WIN') trx.amount = trx.amount / -1 > 0 ? trx.amount : trx.amount * -1;//refund win -
    const session = (await transactionRepository.getSessionBy(`sessionid='${trx.usertoken}'`))[0];
    if (!session) throw "USER NOT FOUND by session:" + trx.usertoken;
    trx.user_id = session.user_id;
    delete trx.usertoken;
    await transactionRepository.save(trx);
    let user = await transactionRepository.getUser(session.user_id);
    user.balance += trx.amount;
    await transactionRepository.updateUser(user);
    let response = { status: 1, balance: user.balance };
    console.log("response", JSON.stringify(response) ) ;
    res.send(response);
  } catch (e) {
    console.log("betResult params", JSON.stringify(trx), "ERROR", e);
    res.status(500).send({ status: 0, message: e });
  }
});

app.post('/ResultBet', async function (req, res) {//apiusoft
  let trx = req.body;
  try {
    let db_trx = null;
    if( trx.usertoken =='' ||  trx.usertoken == null) throw "USERTOKEN_VOID";
    if( trx.trxid =='' ||  trx.trxid == null) throw "TRXID_VOID";
    if( trx.gameid =='' ||  trx.gameid == null) throw "GAMEID_VOID";
    if( trx.referenceBet =='' ||  trx.referenceBet == null) throw "REFERENCEID_VOID";
    if(trx.movement == 'WIN' && trx.amount <= 0) throw 'BAD_AMOUNT';
    let session = (await transactionRepository.getSessionBy(`sessionid='${trx.usertoken}'`))[0];
    if (!session) throw "USER_NOT_FOUND";
    let user = await transactionRepository.getUser(session.user_id);
    if(user.status==0) throw "INACTIVE_USER";
    
    if (trx.trxid == trx.referenceBet) {//vino el bet y win en la misma trx.
      db_trx = { movement: 'BET' };
    } else {
      db_trx = (await transactionRepository.getBy(`trxid='${trx.referenceBet}'`))[0];
      if (!db_trx) throw `REFERENCE_NOT_FOUND`;
    }
    if(trx.movement == 'REFUND' && db_trx.movement == 'BET' && trx.amount <= 0)throw "INVALID_AMOUNT_REFUND_BET";
    if(trx.movement == 'REFUND' && db_trx.movement == 'WIN' && trx.amount >= 0)throw "INVALID_AMOUNT_REFUND_WIN";

    if (trx.movement == 'WIN') trx.amount = trx.amount * 1;
    else if (trx.movement == 'REFUND' && db_trx.movement == 'BET') trx.amount = trx.amount * 1;//refund bet +
    else if (trx.movement == 'REFUND' && db_trx.movement == 'WIN') trx.amount = trx.amount / -1 > 0 ? trx.amount : trx.amount * -1;//refund win -

    user.balance += trx.amount;
    trx.user_id = session.user_id;
    delete trx.sessionid;
    delete trx.usertoken;
    let duplicate_trx = (await transactionRepository.getBy(`trxid='${trx.trxid}'`))[0];
    if(duplicate_trx) throw 'TRX_DUPLICATED';
    await transactionRepository.save(trx);

    await transactionRepository.updateUser(user);
    let response = { status: 1, balance: user.balance, code:'OK-00' };
    console.log("response", JSON.stringify(response) ) ;
    res.send(response);
  } catch (e) {
    console.log("error", e);
    let response = { status: 0, message: e , code:'ERR-00'};
    if(e == 'USER_NOT_FOUND') response.code='BET-01';
    else if(e == 'INACTIVE_USER') response.code='USER-04';
    else if(e == 'BAD_AMOUNT') response.code='BET-05';
    else if(e == 'USERTOKEN_VOID') response.code='REQ-05';
    else if(e == 'INSUFICIENT_BALANCE') response.code='USER-03';
    else if(e == 'TRX_DUPLICATED') response.code='BET-03';
    else if(e == 'TRXID_VOID') response.code='REQ-05';
    else if(e == 'GAMEID_VOID') response.code='REQ-05';
    else if(e == 'INSUFFICIENT_FUNDS') response.code='USER-03';
    else if(e == 'REFERENCEID_VOID') response.code='BET-02';
    else if(e == 'REFERENCE_NOT_FOUND') response.code='BET-02';
    else if(e == 'INVALID_AMOUNT_REFUND_BET') response.code='REQ-05';
    else if(e == 'INVALID_AMOUNT_REFUND_WIN') response.code='REQ-05';
    res.status(500).send(response);
  }
});

//request by Provider
app.post('/getUserBalance', async function (req, res) {
  const params = req.body;
  console.log("Params GetBalance: ", params);
  let condition = `s.sessionid="${params.sessionid}"`;
  try {
    const session = (await transactionRepository.getSessionBy(condition))[0];
    if (!session) throw "User not found " + params.sessionid;
    const user = await transactionRepository.getUser(session.user_id);
    let response = { status: 1, balance: user.balance };
    console.log("getUserBalance response", response);
    res.send(response);
  } catch (e) {
    console.log("getUserBalance params", JSON.stringify(params), "ERROR", e);
    res.status(500).send({ status: 0, message: e });
  }
});

app.post('/ReadBalance', async function(req, res){//apiusoft
  let usertoken = req.body.usertoken;
  try{
    console.log("apiusoft/ReadBalance params", usertoken);
    if(usertoken == '' || usertoken == null) throw "USERTOKEN_VOID"; 

    let session = (await transactionRepository.getSessionBy(`sessionid='${usertoken}'`))[0];
    if (!session) throw "USER_NOT_FOUND";
    let user = await transactionRepository.getUser(session.user_id);
    if(user.status==0) throw "INACTIVE_USER";
    let response = { status: 1, balance: user.balance, code:'OK-00'};
    console.log("apiusoft/ReadBalance response", JSON.stringify(response));
    res.send(response);
  }catch (e) {
    console.log("error", e);
    let response = { status: 0, message: e , code:'ERR-00'};
    if(e == 'USER_NOT_FOUND') response.code='BET-01';
    else if(e == 'USERTOKEN_VOID') response.code='REQ-05';
    else if(e == 'INACTIVE_USER') response.code='USER-04';

    res.status(500).send(response);
  }
});

app.post('/reporter', async function (req, res) {
  try {
    console.log("req.body ", req.body);
    let totalbet = 0;
    let totalwin = 0;
    const params = req.body;
    this.date_init = params.date_init;
    this.date_end = params.date_end;
    this.provider = params.provider;
    params.token = 'Katwan';
    if (params.pass === 'pass2020!') {
      const data = await reporterRepository.reporter(params);
      data.data.data.forEach(e => {
        if (e.movement === 'BET') totalbet = totalbet + e.Monto;
        if (e.movement === 'WIN') totalwin = totalwin + e.Monto;
      });
      let total = totalbet + totalwin;
      console.log("Datos del reporte : ", JSON.stringify(data.data));
      res.render('reporter', { reporters: data.data.data, totalWIN: totalwin, total: total, totalBET: totalbet })
    }
  } catch (e) {
    console.log("Reporter ", "ERROR", e);
    res.status(500).send({ status: 0, message: e });
  }
});
app.get('/reporterbyprov', async function (req, res) {
  try {
    const env = req.query.env || 'dev';
    universalapi = env == env == 'prod' ? PROD_ENV_API_1 : DEV_ENV_API_1 ;
    //solicitud para obtener la lista de proveedores y la lista de monedas
    let providers = await reporterRepository.getProviders(universalapi);
    let reporter = [];
    let today = new Date().toLocaleDateString()
    res.render('reporter_prov', { providers, reporter, today });
  } catch (e) {
    console.log("Reporter ", "ERROR", e);
    res.status(500).send({ status: 0, message: e });
  }
});

app.post('/reporterbyprov', async function (req, res) {
  try {
    let params = req.body;
    const env = req.query.env || 'dev';
    universalapi = env == 'prod' ? PROD_ENV_API_1 : DEV_ENV_API_1 ;
    params.movement = params.show_type == 'total' ? 'total': params.movement ;
    //solicitud para obtener la lista de proveedores y la lista de monedas
    let reporter = await reporterRepository.reporterProvider(params,universalapi);
    let providers = await reporterRepository.getProviders(universalapi);
    console.log("Repor ",reporter);
    let today = new Date().toLocaleDateString()
    res.render('reporter_prov', { providers, reporter,today });
  } catch (e) {
    console.log("Reporter ", "ERROR", e);
    res.status(500).send({ status: 0, message: e });
  }
});



app.listen(3000, function () { console.log('Example app listening on port 3000!'); });