// mocha -t 100000 test/providers/endorphina.js --grep xx
let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;
const assert = require("assert");
chai.use(chaiHttp);
const username = 'demo';
//const URL_J = "https://test-2.apiusoft.com";
const URL_J = "http://localhost:5000";
const URL = "https://test.apiuniversalsoft.com";
const client_auth = "C00172198641DemoLocal";
// const client_ip='::ffff:127.0.0.1';
const client_ip='127.0.0.1';

describe("Client Authentication", ()=>{
    it('WHEN / page SHOULD return 200', (done) => {
    	let params = {};
    	const startTime = process.hrtime();
        chai.request(URL_J).get('/').end( function(err,res){
           if(err) console.log(err);
        	const timeDifference = process.hrtime(startTime);
            console.log(`Request took ${timeDifference[0] + timeDifference[1]/1e9 } seconds`);
            //console.log(`Request took ${timeDifference[0] * 1e9 + timeDifference[1]} nanoseconds`);
            expect(res).to.have.status(200);
            done();
        });
    });
    it('WHEN not send auth SHOULD return ERR', (done) => {
        let params = {};
        const startTime = process.hrtime();
        chai.request(URL_J).post('/UniversalAPI').set({'x-forwarded-for':'127.0.0.1'}).end( function(err,res){
           if(err) console.log(err);
            const timeDifference = process.hrtime(startTime);
            console.log(`Request took ${timeDifference[0] + timeDifference[1]/1e9 } seconds`);
            expect(res).to.have.status(200);
            done();
        });
    });
        
})


/*describe("Create User", ()=>{
        it('WHEN params is ok SHOULD create a new user', (done) => {
        	let params = {};
            chai.request(url).put('/api/client/security/address').send({auth: client,ip:client_ip}).end( function(err,res){
                if(res.statusCode==500){ console.log(res.text);}
                expect(res).to.have.status(200);
                expect(res.body).to.have.property("status").to.equal(1);
                done();
            });
        });
})*/